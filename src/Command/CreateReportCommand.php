<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Report\Output\Collection as OutputHandlers;

class CreateReportCommand extends Command
{
    protected static $defaultName = 'app:create-report';

    protected $outputHandler;

    public function __construct(OutputHandlers $outputHandler) {
        $this->outputHandler = $outputHandler;

        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription('Generates new report using different various handlers')
            ->addOption('output_handler', null, InputOption::VALUE_OPTIONAL, 'Output handler to use', $this->getDefaultOutputHandler())
        ;
    }

    protected function getDefaultOutputHandler(): ?string {
        $availableOutputHandlers = $this->outputHandler->getAvailableHandlers();
        if (count($availableOutputHandlers) > 0) {
            return $availableOutputHandlers[0];
        }

        return null;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $handlerName = $input->getOption('output_handler');
            $outputHandler = $this->outputHandler->get($handlerName);
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return;
        }

        $data = [
            ['title' => 'Row 1'],
            ['title' => 'Row 2'],
            ['title' => 'Row 3']
        ];
        $outputHandler->output($data);

        $output = 'New report generated using at ' . $outputHandler->getOutputFile();
        $output .= ' (using ' .  get_class($outputHandler) . ' handler)';
        $io->success($output);
    }
}
