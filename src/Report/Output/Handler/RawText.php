<?php

namespace App\Report\Output\Handler;

class RawText extends Base
{
    protected $outputFile = 'var/report.txt';

    protected function handleRow(array $row): void
    {
        if (isset($row['title'])) {
            fwrite($this->fileHandler, $row['title'] . PHP_EOL);
        }
    }
}